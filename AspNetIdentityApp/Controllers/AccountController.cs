﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AspNetIdentityApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;

namespace AspNetIdentityApp.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationUserManager UserManager //регистр. взаимодействовать с хранилищем пользователей
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); // получения хранилища
            }
        }

        private IAuthenticationManager AuthenticationManager //логин
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        ////////////////////////////// логин + лог аут
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl; //получаем адрес для возврата и передаем его в преставление
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl) //получаем данные из представления в виде модели LoginModel 
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindAsync(model.Email, model.Password); //и по ним пытаемся получить пользователя из бд с помощью метода UserManager.FindAsync
                if (user == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {   //AspNet Identity использует аутентификацию на основе объектов claim. Поэтому нам надо сначала создать объект ClaimsIdentity, который представляет реализацию интерфейса IIdentity в AspNet Identity. Для создания ClaimsIdentity применяется метод CreateIdentityAsync()
                    ClaimsIdentity claim = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, claim); //передается объект конфигурации аутентификации AuthenticationProperties, а также ранее созданный объект ClaimsIdentity. Свойство IsPersistent позволяет сохранять аутентификационные данные в браузере даже после закрытия пользователем браузера
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Home");
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            return View(model);
        }

        public ActionResult Logout() //просто удаляет аутентификационные куки в браузере, как бы делая выход из системы.
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        ////////////////////////////// регистр
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { UserName = model.Email, Email = model.Email, Year = model.Year };
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }

        /////////////////// 4. Редактирование и удаление пользователей
        [HttpGet]
        public ActionResult Delete() //отображают пользователю представление для удаления и принимают выбор пользователя об удалении.
        {
            return View();
        }

        [HttpPost]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed() //отображают пользователю представление для удаления и принимают выбор пользователя об удалении.
        {
            ApplicationUser user = await UserManager.FindByEmailAsync(User.Identity.Name);
            if (user != null)
            {
                IdentityResult result = await UserManager.DeleteAsync(user); //метод для удаления (возвращает объект IdentityResult, который позволяет отследить успех операции).
                if (result.Succeeded)
                {
                    return RedirectToAction("Logout", "Account");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Edit() //отображает представление для редактирования, передавая в него модель EditModel.
        {
            ApplicationUser user = await UserManager.FindByEmailAsync(User.Identity.Name);
            if (user != null)
            {
                EditModel model = new EditModel { Year = user.Year };
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditModel model) //POST-версия метода принимает данные модели и устанавливает значения ее свойств для пользователя
        {
            ApplicationUser user = await UserManager.FindByEmailAsync(User.Identity.Name);
            if (user != null)
            {
                user.Year = model.Year;
                IdentityResult result = await UserManager.UpdateAsync(user); // метод для Редактирования
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Что-то пошло не так");
                }
            }
            else
            {
                ModelState.AddModelError("", "Пользователь не найден");
            }

            return View(model);
        }
    }
}