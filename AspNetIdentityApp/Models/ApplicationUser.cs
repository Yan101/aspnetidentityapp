﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace AspNetIdentityApp.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int Year { get; set; }
        public ApplicationUser()
        {

        }
    }
}