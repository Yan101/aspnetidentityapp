﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using AspNetIdentityApp.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(AspNetIdentityApp.App_Start.Startup))]

namespace AspNetIdentityApp.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) //Интерфейс IAppBuilder определяет множество методов, в данном случае нам достаточно трех методов
        {
            // настраиваем контекст и менеджер
            app.CreatePerOwinContext<ApplicationContext>(ApplicationContext.Create); //Метод CreatePerOwinContext регистрирует в OWIN менеджер пользователей ApplicationUserManager и класс контекста ApplicationContext.
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // 4 регистрация менеджера ролей
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions //Метод UseCookieAuthentication с помощью объекта CookieAuthenticationOptions устанавливает аутентификацию на основе куки в качестве типа аутентификации в приложении. А свойство LoginPath позволяет установить адрес URL, по которому будут перенаправляться неавторизованные пользователи. Как правило, это адрес /Account/Login.
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });

        }
    }
}
